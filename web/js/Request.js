function getTasks(formData){
	return fetchPost('index.php?r=task%2Fget_tasks', formData);		
}

function saveTask(formData){
	return fetchPost('index.php?r=task%2Fsave', formData);	
}

function deleteTask(formData){
	return fetchPost('index.php?r=task%2Fdelete', formData);		
}

function changeTaskStatus(formData){
	return fetchPost('index.php?r=task/change-status', formData);	
}

function getTaskLists(){
	return fetchGet('index.php?r=task-list/get-lists');
}

function saveTaskList(formData){
	return fetchPost('index.php?r=task-list/save', formData);	
}

function deleteTaskList(formData){
	return fetchPost('index.php?r=task-list/delete', formData);
}

function ordenateList(formData){
	return fetchPost('index.php?r=task-list/ordenate', formData);
}

function ordenateTask(formData){
	return fetchPost('index.php?r=task/ordenate', formData);
}

function fetchPost(url, formData){
	return fetch(url, {
		method : 'POST',
		body : formData
	}).then(response => response.json()).then(data => {
		return data;
	});
}

function fetchGet(url){
	return fetch(url, {
		method : 'GET',		
	}).then(response => response.json()).then(data => {
		return data;
	});		
}