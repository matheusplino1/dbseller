$(document).ready(() => {
	const 		
		$newTaskListContainer = document.querySelector('.new-task-list-container'),
		$btnNewTaskList = $newTaskListContainer.querySelector('.btn-new-task-list'),		
		$inputTaskList = $newTaskListContainer.querySelector('.new-task-list-name-input'),
		$saveTaskListButton = $newTaskListContainer.querySelector('#btn-save-new-task-list'),
		$cancelTaskListButton = $newTaskListContainer.querySelector('#btn-cancel-new-task-list');

	$btnNewTaskList.addEventListener('click', event => {
		$newTaskListContainer.classList.remove('new-task-list-iddle');
		$inputTaskList.focus();
	});

	$inputTaskList.addEventListener('blur', event => {
		setTimeout(() => {
			$newTaskListContainer.classList.add('new-task-list-iddle');
		}, 150);
	});

	$saveTaskListButton.addEventListener('click', event => {
		const formData = new FormData();
		formData.append('nome', $inputTaskList.value);

		saveTaskList(formData).then(data => {
			if(!!data.success){
				if(!!data.taskList){
					const $taskListElement = createTaskListElement(data.taskList);
					$newTaskListContainer.parentElement.insertBefore($taskListElement, $newTaskListContainer);
					
					initTaskListHandlers($taskListElement);
				}
			} else {
				console.log('erro ao salvar lista');
			}
		});
	});

	$cancelTaskListButton.addEventListener('click', event => {
		$inputTaskList.value = "";
	});

	getTaskLists().then(data => {
		for(let taskList of data){
			const $taskListElement = createTaskListElement(taskList);
			$newTaskListContainer.parentElement.insertBefore($taskListElement, $newTaskListContainer);
			
			initTaskListHandlers($taskListElement);
			initNewTaskHandlers($taskListElement);

			const formData = new FormData();
			formData.append('task_list', taskList.id);

			getTasks(formData).then(data => {				
				for(let task of data){
					const $taskElement = createTaskElement(task);
					$taskListElement.querySelector('.task-list-body').appendChild($taskElement);
					
					initTaskHandlers($taskElement);
				}
			});
		}
	});	

	//Inicia o sortable
	$($newTaskListContainer.parentElement).sortable({
    	handle: ".task-list-title-text",
    	cancel: ".btn-edit-task-list",
    	update: (event, ui) => {    		
    		const formData = new FormData();
			formData.append('id', ui.item.get(0).dataset.taskListId);
			formData.append('order', Array.prototype.indexOf.call(event.target.childNodes, ui.item.get(0)) - 1);

    		ordenateList(formData).then(data => {
    			if(!!data.success)
    				console.log('Ordenado com sucesso');
    		});
    	},      
    });
});