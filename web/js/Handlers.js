function initTaskListHandlers($taskList){
	const
		$titleContainer = $taskList.querySelector('.task-list-title-container'),
		$titleText = $titleContainer.querySelector('.task-list-title-text'),
		$input 	   = $titleContainer.querySelector('input'),
		$editButon = $titleContainer.querySelector('.btn-edit-task-list'),
		$saveButton = $titleContainer.querySelector('.btn-save-task-list'),
		$deleteButton = $titleContainer.querySelector('.btn-delete-task-list'),
		$cancelButton = $titleContainer.querySelector('.btn-cancel-task-list');

	$editButon.addEventListener('click', event => {
		$titleContainer.classList.remove('task-list-title-container-iddle');
		$input.focus();				
	});

	$input.addEventListener('blur', event => {
		setTimeout(() => {
			$titleContainer.classList.add('task-list-title-container-iddle');
		}, 150);
	});

	$saveButton.addEventListener('click', event => {
		$titleText.innerText = $input.value;

		const formData = new FormData();
		formData.append('nome', $input.value);
		formData.append('id', $taskList.dataset.taskListId);

		saveTaskList(formData).then(data => {
			console.log('task salva com sucesso.');
		});
	});

	$deleteButton.addEventListener('click', event => {
		const response = confirm("Tem certeza que quer deletar a lista?\nAo deletar a lista você exluirá todas tarefas relacionadas a ela.");
		if (!!response) {
			const formData = new FormData();
			formData.append('id', $taskList.dataset.taskListId);

			deleteTaskList(formData).then(data => {
				if(!!data.success){
					$taskList.remove();
					console.log('Lista removida com sucesso');		
				} else {
					console.log('Erro ao remover lista');
				}				
			});    
		} else {
		    $input.value = $titleText.innerText;
		}
	});	

    //Inicia o sortable
	$($taskList.querySelector('.task-list-body')).sortable({
      connectWith: ".task-list-body",
    	handle: ".task-text",
    	cancel: ".btn-edit-task, btn-status-task",
    	update: (event, ui) => {
    		const 
    			formData = new FormData(),
    			$task     = ui.item.get(0);

			formData.append('id', $task.dataset.id);
			formData.append('order', Array.prototype.indexOf.call($task.parentElement.childNodes, $task));
			formData.append('task_list', $task.closest('.task-list').dataset.taskListId);

    		ordenateTask(formData).then(data => {
    			if(!!data.success)
    				console.log('Ordenado com sucesso');
    		});
    	},
      // placeholder: "portlet-placeholder ui-corner-all"
    });
}

function initNewTaskHandlers($taskList){
	const 		
		$newTaskContainer = $taskList.querySelector('.new-task-container'),
		$btnNewTask = $newTaskContainer.querySelector('.btn-new-task'),		
		$input = $newTaskContainer.querySelector('.new-task-name-input'),
		$saveTaskButton = $newTaskContainer.querySelector('.btn-save-new-task'),
		$cancelTaskButton = $newTaskContainer.querySelector('.btn-cancel-new-task');
			
	$btnNewTask.addEventListener('click', event => {
		$newTaskContainer.classList.remove('new-task-container-iddle');
		$input.focus();
	});

	$input.addEventListener('blur', event => {
		setTimeout(() => {
			$newTaskContainer.classList.add('new-task-container-iddle');
		}, 150);
	});

	$saveTaskButton.addEventListener('click', event => {
		const formData = new FormData();
		formData.append('task_list', $taskList.dataset.taskListId);
		formData.append('nome', $input.value);

		saveTask(formData).then(data => {
			if(!!data.success){
				if(!!data.task){
					const $taskElement = createTaskElement(data.task);
					$taskList.querySelector('.task-list-body').appendChild($taskElement);
					
					initTaskHandlers($taskElement);
				}
			} else {
				console.log('Erro ao salvar Tarefa');
			}				
		});
	});

	$cancelTaskButton.addEventListener('click', event => {
		$input.value = "";
	});
}

function initTaskHandlers($task){
	const
		$taskText = $task.querySelector('.task-text'),
		$inputTask = $task.querySelector('.task-name-input'),
		$editTaskButton = $task.querySelector('.btn-edit-task'),
		$statusTaskButton = $task.querySelector('.btn-status-task'),
		$saveTaskButton = $task.querySelector('.btn-save-task'),
		$deleteTaskButton = $task.querySelector('.btn-delete-task'),
		$cancelTaskButton = $task.querySelector('.btn-cancel-task');

	$editTaskButton.addEventListener('click', event => {
		$task.classList.remove('task-iddle');
		$inputTask.focus();
	});

	$inputTask.addEventListener('blur', event => {
		setTimeout(() => {
			$task.classList.add('task-iddle');
		}, 150);
	});

	$statusTaskButton.addEventListener('click', event => {
		const formData = new FormData();
		formData.append('id', $task.dataset.id);
		changeTaskStatus(formData).then(data => {
			if($task.classList.contains('task-finished')){
				$task.classList.remove('task-finished');
			} else {
				$task.classList.add('task-finished');
			}
		});
	});

	$saveTaskButton.addEventListener('click', event => {
		$taskText.innerText = $inputTask.value;

		const formData = new FormData();
		formData.append('nome', $inputTask.value);
		formData.append('id', $task.dataset.id);		

		saveTask(formData).then(data => {
			if(!!data.success){
				console.log('Tarefa salva com sucesso');
			} else {
				console.log('Erro ao salvar tarefa');
			}
		});
	});

	$cancelTaskButton.addEventListener('click', event => {
		$inputTask.value = $taskText.innerText;
	});

	$deleteTaskButton.addEventListener('click', event => {		
		const formData = new FormData();
		formData.append('id', $task.dataset.id);

		deleteTask(formData).then(data => {
			if(!!data.success){
				$task.remove();
				console.log('Tarefa removida com sucesso');		
			} else {
				console.log('Erro ao excluir tarefa');
			}				
		});
	});	
}