function getIcon(iconClass){
	const $icon = document.createElement('i');

	$icon.classList.add(...iconClass);
	return $icon;
}

function getBtnIcon(btnClass, iconClass){
	const $btn = document.createElement('a');
	
	$btn.classList.add(...btnClass);
	$icon = getIcon(iconClass);
	$btn.appendChild($icon);

	return $btn;
}

function createTaskListElement(taskList){
	const
		$container  	 		= document.createElement('div'),
		$taskListTitleContainer = document.createElement('div'),
		$tasksListBodyContainer = document.createElement('div'),
		$taskListTitleText 	 	= document.createElement('span'),	
		$updateContainer 		= document.createElement('div'),
		$input 			 		= document.createElement('input'),
		$btnEdit				= getBtnIcon(['btn', 'btn-edit-task-list'], ['glyphicon', 'glyphicon-pencil']),
		$btnConfirm		 		= getBtnIcon(['btn', 'btn-sm', 'btn-success', 'btn-save-task-list'], ['glyphicon', 'glyphicon-ok']),
		$btnDelete  	 		= getBtnIcon(['btn', 'btn-sm', 'btn-delete', 'btn-delete-task-list'], ['glyphicon', 'glyphicon-trash']),
		$btnCancel  	 		= getBtnIcon(['btn', 'btn-sm', 'btn-cancel', 'btn-cancel-task-list'], ['glyphicon', 'glyphicon-remove']);

	$container.dataset.taskListId = taskList.id;
	$input.value = taskList.nome;

	$container.classList.add('task-list');
	$taskListTitleContainer.classList.add('task-list-title-container', 'task-list-title-container-iddle');
	$taskListTitleText.classList.add('task-list-title-text');
	$tasksListBodyContainer.classList.add('task-list-body');
	$updateContainer.classList.add('task-list-title-update-container');

	$taskListTitleText.appendChild(document.createTextNode(taskList.nome));
	$taskListTitleContainer.appendChild($taskListTitleText);
	$taskListTitleContainer.appendChild($btnEdit);

	$updateContainer.appendChild($input);
	$updateContainer.appendChild($btnConfirm);
	$updateContainer.appendChild($btnDelete);
	$updateContainer.appendChild($btnCancel);

	$taskListTitleContainer.appendChild($updateContainer);

	$container.appendChild($taskListTitleContainer);
	$container.appendChild($tasksListBodyContainer);
	$container.appendChild(createNewTaskElement());

	return $container;
}

function createNewTaskElement(){
	const
		$container  	 		= document.createElement('div'),
		$taskText 		 		= document.createElement('span'),
		$taskInputContainer		= document.createElement('div'),
		$input 			 		= document.createElement('input'),
		$plusIcon				= getIcon(['glyphicon', 'glyphicon-plus']),
		$btnConfirm		 		= getBtnIcon(['btn', 'btn-sm', 'btn-success', 'btn-save-new-task'], ['glyphicon', 'glyphicon-ok']),
		$btnCancel  	 		= getBtnIcon(['btn', 'btn-sm', 'btn-cancel', 'btn-cancel-new-task'], ['glyphicon', 'glyphicon-remove']);

	$container.classList.add('new-task-container', 'new-task-container-iddle');
	$taskInputContainer.classList.add('new-task-input-field');
	$taskText.classList.add('btn-new-task');
	$input.classList.add('new-task-name-input');

	$taskText.appendChild($plusIcon);
	$taskText.appendChild(document.createTextNode(' Nova Tarefa'));
	$container.appendChild($taskText);
	$taskInputContainer.appendChild($input);
	$taskInputContainer.appendChild($btnConfirm);
	$taskInputContainer.appendChild($btnCancel);
	$container.appendChild($taskInputContainer);

	return $container;
}

function createTaskElement(task){
	const
		$container  	 = document.createElement('div'),
		$updateContainer = document.createElement('div'),
		$taskText   	 = document.createElement('span'),
		$input 			 = document.createElement('input'),
		$btnEdit	 	 = getBtnIcon(['btn', 'btn-edit-task'], ['glyphicon', 'glyphicon-pencil']),
		$btnFinish	  	 = document.createElement('a'),		
		$btnConfirm		 = getBtnIcon(['btn', 'btn-sm', 'btn-success', 'btn-save-task'], ['glyphicon', 'glyphicon-ok']),
		$btnDelete  	 = getBtnIcon(['btn', 'btn-sm', 'btn-delete', 'btn-delete-task'], ['glyphicon', 'glyphicon-trash']),
		$btnCancel  	 = getBtnIcon(['btn', 'btn-sm', 'btn-cancel', 'btn-cancel-task'], ['glyphicon', 'glyphicon-remove']);		

	$input.value = task.nome;
	$container.dataset.id = task.id;

	if(task.status == 1){
		$container.classList.add('task-finished');
	}

	$container.classList.add('task', 'task-iddle');
	$taskText.classList.add('task-text');
	$updateContainer.classList.add('task-update');	
	$input.classList.add('task-name-input');
	$btnFinish.classList.add('btn', 'btn-status-task');

	$updateContainer.appendChild($input);
	$updateContainer.appendChild($btnConfirm);
	$updateContainer.appendChild($btnFinish);
	$updateContainer.appendChild($btnCancel);
	$updateContainer.appendChild($btnDelete);
	$taskText.appendChild(document.createTextNode(task.nome));	
	$container.appendChild($taskText);
	$container.appendChild($btnEdit);	
	$container.appendChild($updateContainer);	

	return $container;
}