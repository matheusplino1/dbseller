function getCompleteUrl(url){	
	
	const pathName = location.pathname;
	let newPathName = pathName.replace(pathName, "/" + url);

	if (typeof url === "undefined") {
		let url = '/';
	}
	  
	newPathName = newPathName.replace("//", "/");

	return location.protocol + location.hostname + ':' + location.port + newPathName;	  
}