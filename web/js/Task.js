/**
*	@Class Task
*	@Description
*		Classe de Task
*	@Since 01/06/2018
**/
const Task = (() => {
	'use strict';	
	/**
	#########################################
	#####	Private Properties
	#########################################
	**/
		const
			/**
			*	@propertie _scopePrivate
			*	@Description
			*		Store the private scope of the class
			*	@Type WeakMap
			**/
			_scopePrivate = new WeakMap();

	/**
	#########################################
	#####	Private Functions
	#########################################
	**/
		/**
		*	@method _getPrivateMap
		*	@Description
		*		Create a new map
		*	@param {Map} map - A new map
		**/
		function _getPrivateMap(){
			return new Map();
		}

	/**
	#########################################
	#####	Class
	#########################################
	**/
		class Task {
			/**
			*	@method Constructor
			*	@Description
			*		Construtor da classe
			*	@param {String} nome - O nome da Task
			*	@param {Number} order - A ordem da Task 
			*	@param {Number} status - O estado da Task
			*	@param {String} parent_id - O id da Task "Pai"
			**/
			constructor(nome, order, status, parentTaskID = null){
				const
					props = _getPrivateMap();

				props
					.set('nome', nome)
					.set('parentTask', parentTaskID)
					.set('order', order)
					.set('status', status);

				_scopePrivate.set(this, props);
			}

			/**
			*	@method toString
			*	@Description 
			*		Método toString da instância
			*	@return {String} string - Uma string com as propriedades da instancia
			**/
			toString(){
				const props = _scopePrivate.get(this);

				return `Task { 
					Nome: ${props.get('nome')}
					Task Pai: ${props.get('parenTask')}
					Ordem: ${props.get('order')}
					Status: ${props.get('status')}					
				}`; 
			}			

			/**
			#######################################
			#### Getters and Setters 
			#######################################
			**/				
				get nome(){
					return _scopePrivate.get(this).get('nome');
				}

				set nome(nome) {
					_scopePrivate.get(this).set('nome', nome);
				}

				get parentTask(){
					return _scopePrivate.get(this).get('parentTask');
				}

				set parentTask(parentTaskID){
					_scopePrivate.get(this).set('parentTask', parentTaskID);
				}

				get ordem(){
					return _scopePrivate.get(this).get('order');
				}

				set ordem(order){
					_scopePrivate.get(this).set('order', prder);
				}

				get status(){
					return _scopePrivate.get(this).get('status');
				}

				set status(status){
					_scopePrivate.get(this).set('status', status);
				}
		}

	return Task;
})();