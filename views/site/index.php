<?php
use app\assets\TaskAsset;

/* @var $this yii\web\View */

TaskAsset::register($this);

$this->title = 'Task Manager';
?>
<div class="site-index">
	 <div class="body-content">
        <div class="row">        	
            <div class="new-task-list-container new-task-list-iddle">
                <span class='btn-new-task-list'><i class="glyphicon glyphicon-plus"></i> Nova Lista</span>
                <div class="new-task-list-input-field">
                    <input class="new-task-list-name-input" placeholder="Nome da lista..." autocomplete="off">
                    <a class="btn btn-sm btn-success" id="btn-save-new-task-list"><i class="glyphicon glyphicon-ok"></i></a>
                    <a class="btn btn-sm" id="btn-cancel-new-task-list"><i class="glyphicon glyphicon-remove"></i></a>
                </div>                  
            </div>        		      
        </div>
    </div>
</div>