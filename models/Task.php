<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $task_list
 * @property int $parent_task
 * @property string $nome
 * @property int $order
 * @property int $status
 *
 * @property Task $parentTask
 * @property Task[] $tasks
 * @property TaskList $taskList
 */
class Task extends \yii\db\ActiveRecord
{
    const TASK_OPEN = 0;
    const TASK_COMPLETE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_list', 'nome'], 'required'],
            [['task_list', 'parent_task', 'order', 'status'], 'integer'],
            [['nome'], 'string', 'max' => 255],
            [['parent_task'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['parent_task' => 'id']],
            [['task_list'], 'exist', 'skipOnError' => true, 'targetClass' => TaskList::className(), 'targetAttribute' => ['task_list' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_list' => 'Task List',
            'parent_task' => 'Parent Task',
            'nome' => 'Nome',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'parent_task']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['parent_task' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskList()
    {
        return $this->hasOne(TaskList::className(), ['id' => 'task_list']);
    }
}
