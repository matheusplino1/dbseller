<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Task;

class TaskController extends Controller
{    

    public function beforeAction($action) { 
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Deleta um registro de Task buscando todas as task vinculadas a task excluída, excluindo-as
     */
    public function actionDelete(){    
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $id = $data['id'];
        $task = Task::findOne($id);
        $response = [
            'success' => false
        ];

        if($task){                     
            $childTasks = Task::find()->where(['parent_task' => $id])->all();                
            foreach ($childTasks as $childTask) {
                $childTask->delete();
            }
            $task->delete();
            $response = [
                'success' => true
            ];
        }
        return $response;           
    }

    /**
     * Salva os dados de uma task
     * Se id for passado, salva em uma existente. Caso contrario, salva uma nova task.
     */
    public function actionSave(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $response = [
            'success' => false
        ];

        $id = isset($data['id']) ? $data['id'] : null;
        if($id){
            $model = Task::findOne($id);
        } else {
            $model = new Task();
            $modelLastOrder = Task::find()->orderBy('order desc')->one();
            $model->order = (isset($modelLastOrder)) ? $modelLastOrder->order + 1 : 0; 
            $model->task_list = $data['task_list'];
        }

        $model->nome = $data['nome'];                

        if($model->save()){
            $response = [
                'success' => true,
                'task' => $model
            ];
        }

        return $response;
    }

    function actionGet_tasks(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $tasks = Task::find()->where(['task_list' => $data['task_list']])->orderBy('order')->all();        
        return $tasks;
    }

    /**
     * Muda o status de uma task existente
     */
    public function actionChangeStatus(){
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $response = [
            'success' => false
        ];
        $model = Task::findOne($data['id']);
        $model->status = ($model->status == Task::TASK_OPEN) ? Task::TASK_COMPLETE : Task::TASK_OPEN;
        
        if($model->save()){
            return $response['success'] = true;
        }

        return $response;
    }

    /**
     * Agrupa uma tarefa como filha de outra
     */
    public function actionAddParent($id, $parent_id){
        $model = Task::findOne($id);
        $parentTask = Task::findOne($parent_id);

        if($model && $parentTask){
            $model->parent_task = $parentTask->id;
            $model->save();
        }
    }

    /**
     * Salva no banco a ordem das tarefas
     */
    public function actionOrdenate(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $task = Task::findOne($data['id']);
        $list_id = $data['task_list'];
        
        //Se for para outra lista, ordena a lista antiga
        if($list_id != $task->task_list){
            $order = 0;
            $tasks = Task::find()->where(['task_list' => $list_id])->all();
            foreach($tasks as $model){
                $model->order = $order;
                $model->save();
                $order++;
            }

            $order = $data['order']; 
            $tasksOrdenate = Task::find()->where(['and', ['>=', 'order', $order], ['<>', 'id', $task->id]])->andWhere(['task_list' => $data['task_list']])->orderBy('order')->all();
            
            $task->order = $order;
            $task->task_list = $data['task_list'];
            $task->save();
            $order++; 

            foreach($tasksOrdenate as $model){
                $model->order = $order;
                $model->save();
                $order++;
            }
        } else {
            $beforeSignal = ($task->order > $data['order']) ? '<' : '<=';        
            $afterSignal = ($task->order > $data['order']) ? '>=' : '>';        
            $order = 0;
            $tasksBefore = Task::find()->where(['and', [$beforeSignal, 'order', $data['order']], ['<>', 'id', $task->id]])->andWhere(['task_list' => $data['task_list']])->orderBy('order')->all();
            $tasksAfter = Task::find()->where(['and', [$afterSignal, 'order', $data['order']], ['<>', 'id', $task->id]])->andWhere(['task_list' => $data['task_list']])->orderBy('order')->all();

            foreach($tasksBefore as $model){
                $model->order = $order;
                $model->save();
                $order++;
            }

            $task->order = $order;
            $task->save();
            $order++;        

            foreach($tasksAfter as $model){
                $model->order = $order;
                $model->save();
                $order++;            
            }

        }        

        return $response = ['success' => true];
    }
}
