<?php 
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Task;
use app\models\TaskList;

class TaskListController extends Controller
{    

    public function beforeAction($action) { 
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * 
     */
    public function actionDelete(){    
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $id = $data['id'];
        $list = TaskList::findOne($id);
        $response = [
            'success' => false
        ];

        if($list){
            $tasks = Task::find()->where(['task_list' => $list->id])->all();
            //tem que usar recursividade aqui para nao dar problemas com tarefas aninhadas
            foreach ($tasks as $task) {
                //como é usado recursividade, checka antes de chamar a função se a task ainda existe no banco
                $auxTask = Task::findOne($task->id);
                if($auxTask){
                    $this->recursiveTaskDelete($task, $list->id);
                }
            }

            $list->delete();
            $response['success'] = true;
        }
        return $response;
    }

    function recursiveTaskDelete($task, $taskListId){        
        $findTask = Task::findOne(['task_list' => $taskListId, 'parent_task' => $task]);
        if($findTask){
            $this->recursiveTaskDelete($findTask, $taskListId);
        } 
        $task->delete();
    }

    /**
     * 
     */
    public function actionSave(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $response = [
            'success' => false
        ];

        $id = isset($data['id']) ? $data['id'] : null;
        if($id){
            $model = TaskList::findOne($id);
        } else {
            $model = new TaskList();
            $modelLastOrder = TaskList::find()->orderBy('order desc')->one();
            $model->order = (isset($modelLastOrder)) ? $modelLastOrder->order + 1 : 0; 
        }

        $model->nome = $data['nome'];

        if($model->save()){
            $response = [
                'success' => true,
                'taskList' => $model
            ];
        }
        return $response;
    }

    function actionGetLists(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = TaskList::find()->orderBy('order')->all();
        return $list;
    }    

    public function actionOrdenate(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->post();
        $taskList = TaskList::findOne($data['id']);
        $beforeSignal = ($taskList->order > $data['order']) ? '<' : '<=';        
        $afterSignal = ($taskList->order > $data['order']) ? '>=' : '>';        
        $order = 0;
        $taskListsBefore = TaskList::find()->where(['and', [$beforeSignal, 'order', $data['order']], ['<>', 'id', $taskList->id]])->orderBy('order')->all();
        $taskListsAfter = TaskList::find()->where(['and', [$afterSignal, 'order', $data['order']], ['<>', 'id', $taskList->id]])->orderBy('order')->all();        

        foreach($taskListsBefore as $taskListAux){
            $taskListAux->order = $order;
            $taskListAux->save();
            $order++;
        }

        $taskList->order = $order;
        $taskList->save();
        $order++;        

        foreach($taskListsAfter as $taskListAux){
            $taskListAux->order = $order;
            $taskListAux->save();
            $order++;            
        }

        return $response = ['success' => true];
    }
}
