<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Projeto DBSeller Matheus Puppo Lino</h1>
    <br>
</p>

Projeto feito para seleção da empresa DBSeller. 

Consiste em um sistema de tarefas, baseado no Trello.

Contato
-------------------

matheusplino@gmail.com

Estrutura de Diretórios
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUERIMENTOS
------------

PHP ^7.0 
Banco de dados (Utilizado no projeto original: MySQL)
Composer


INSTALAÇÃO
------------

Clonar a pasta do repositório ou baixar por https diretamente pelo link

Clone:

~~~
git clone git@gitlab.com:matheusplino1/dbseller.git
~~~

Link:
~~~
https://gitlab.com/matheusplino1/dbseller
~~~

Criar um banco para ser usado no projeto

CONFIGURATION
-------------

### Banco de dados

Edite o arquivo `config/db.php` com os dados do banco de dados que criou, por exemplo:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Executar o seguinte comando na pasta raiz do projeto para instalar as depências:
~~~
composer install
~~~

Executar o comando para criar as tabelas utilizadas pelo projeto:
~~~
php yii migrate/up
~~~

Após esse passo a passo, você ja pode abrir o projeto.