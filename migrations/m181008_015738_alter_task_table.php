<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m181008_015738_alter_task_table
 */
class m181008_015738_alter_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'task_list',  Schema::TYPE_INTEGER . '(11) NOT NULL AFTER id');
        $this->addForeignKey('fk_task_list', 'task', 'task_list', 'task_list', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {        
        $this->dropForeignKey('fk_list', 'task');
        $this->dropColumn('task', 'task_list');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181008_015738_alter_task_table cannot be reverted.\n";

        return false;
    }
    */
}
