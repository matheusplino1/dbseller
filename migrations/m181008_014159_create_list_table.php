<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `list`.
 */
class m181008_014159_create_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task_list', [
            'id'    => $this->primaryKey(),
            'nome'  => Schema::TYPE_STRING . ' NOT NULL',
            'order' => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('list');
    }
}
