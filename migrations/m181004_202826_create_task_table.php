<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m181004_202826_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id'            => Schema::TYPE_PK,
            'parent_task'   => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL',
            'nome'          => Schema::TYPE_STRING . ' NOT NULL',
            'order'         => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL',
            'status'        => Schema::TYPE_SMALLINT . '(6) NOT NULL DEFAULT 0'
        ]);

        $this->addForeignKey('fk_parent_task', 'task', 'parent_task', 'task', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_parent_task', 'task');
        $this->dropTable('task');
    }
}
